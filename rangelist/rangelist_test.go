package rangelist

import "testing"

func Test_Add1(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})

	if rl.Print() != "[1, 5)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Add2(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})

	if rl.Print() != "[1, 5) [10, 20)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Add3(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})

	if rl.Print() != "[1, 5) [10, 20)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Add4(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})
	_ = rl.Add([2]int{20, 21})

	if rl.Print() != "[1, 5) [10, 21)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Add5(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})
	_ = rl.Add([2]int{20, 21})
	_ = rl.Add([2]int{2, 4})

	if rl.Print() != "[1, 5) [10, 21)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Add6(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})
	_ = rl.Add([2]int{20, 21})
	_ = rl.Add([2]int{2, 4})
	_ = rl.Add([2]int{3, 8})

	if rl.Print() != "[1, 8) [10, 21)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Remove1(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})
	_ = rl.Add([2]int{20, 21})
	_ = rl.Add([2]int{2, 4})
	_ = rl.Add([2]int{3, 8})
	_ = rl.Remove([2]int{10, 10})

	if rl.Print() != "[1, 8) [10, 21)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Remove2(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})
	_ = rl.Add([2]int{20, 21})
	_ = rl.Add([2]int{2, 4})
	_ = rl.Add([2]int{3, 8})
	_ = rl.Remove([2]int{10, 10})
	_ = rl.Remove([2]int{10, 11})

	if rl.Print() != "[1, 8) [11, 21)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Remove3(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})
	_ = rl.Add([2]int{20, 21})
	_ = rl.Add([2]int{2, 4})
	_ = rl.Add([2]int{3, 8})
	_ = rl.Remove([2]int{10, 10})
	_ = rl.Remove([2]int{10, 11})
	_ = rl.Remove([2]int{15, 17})

	if rl.Print() != "[1, 8) [11, 15) [17, 21)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Test_Remove4(t *testing.T) {
	rl := RangeList{}
	_ = rl.Add([2]int{1, 5})
	_ = rl.Add([2]int{10, 20})
	_ = rl.Add([2]int{20, 20})
	_ = rl.Add([2]int{20, 21})
	_ = rl.Add([2]int{2, 4})
	_ = rl.Add([2]int{3, 8})
	_ = rl.Remove([2]int{10, 10})
	_ = rl.Remove([2]int{10, 11})
	_ = rl.Remove([2]int{15, 17})
	_ = rl.Remove([2]int{3, 19})

	if rl.Print() != "[1, 3) [19, 21)" {
		t.Error("Fail")
	} else {
		t.Log("Pass")
	}
}

func Benchmark_Add(b *testing.B) {
	rl := RangeList{}
	for i := 0; i < b.N; i++ {
		_ = rl.Add([2]int{1, 5})
		_ = rl.Add([2]int{10, 20})
		_ = rl.Add([2]int{20, 20})
		_ = rl.Add([2]int{20, 21})
		_ = rl.Add([2]int{2, 4})
		_ = rl.Add([2]int{3, 8})
		_ = rl.Remove([2]int{10, 10})
		_ = rl.Remove([2]int{10, 11})
		_ = rl.Remove([2]int{15, 17})
		_ = rl.Remove([2]int{3, 19})
	}
}
