package rangelist

import (
	"errors"
	"fmt"
	"strings"
)

type RangeList struct {
	Range [][2]int
}

func validateRangeInput(rangeElement [2]int) error {
	if rangeElement[0] > rangeElement[1] {
		return errors.New(fmt.Sprintf("Invalid input range element: %v", rangeElement))
	}
	return nil
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

//Compare range A and B
//Return:
// 	1  : A > B
// 	0  : A and B collision
// 	-1 : A < B
func compareRange(rangeElementA, rangeElementB [2]int) int {
	if rangeElementA[1] < rangeElementB[0] {
		return -1
	}
	if rangeElementA[0] > rangeElementB[1] {
		return 1
	}
	return 0
}

//Merge range A and B when collision
func mergeRange(rangeElementA, rangeElementB [2]int) [2]int {
	return [2]int{min(rangeElementA[0], rangeElementB[0]), max(rangeElementA[1], rangeElementB[1])}
}

//Exclude range when collision
func excludeRange(rangeElement, exclude [2]int) [][2]int {
	var res [][2]int

	if rangeElement[0] < exclude[0] {
		res = append(res, [2]int{rangeElement[0], exclude[0]})
	}
	if rangeElement[1] > exclude[1] {
		res = append(res, [2]int{exclude[1], rangeElement[1]})
	}

	return res
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	if err := validateRangeInput(rangeElement); err != nil {
		return err
	}

	var updateRangeList [][2]int

	// left part range of rangeList
	var left [][2]int

	length := len(rangeList.Range)
	for k, v := range rangeList.Range {

		compareResult := compareRange(v, rangeElement)
		if compareResult == -1 {
			updateRangeList = append(updateRangeList, v)
			continue
		}

		if compareResult == 1 {
			// Optimize performance
			//left = append(left, rangeList.Range[k:length]...)
			left = rangeList.Range[k:length]

			break
		}

		if compareResult == 0 {
			rangeElement = mergeRange(v, rangeElement)
			continue
		}
	}

	updateRangeList = append(updateRangeList, rangeElement)
	updateRangeList = append(updateRangeList, left...)
	rangeList.Range = updateRangeList

	return nil
}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	if err := validateRangeInput(rangeElement); err != nil {
		return err
	}

	var updateRangeList [][2]int

	// left part range of rangeList
	var left [][2]int

	length := len(rangeList.Range)
	for k, v := range rangeList.Range {

		compareResult := compareRange(v, rangeElement)
		if compareResult == -1 {
			updateRangeList = append(updateRangeList, v)
			continue
		}

		if compareResult == 1 {
			// Optimize performance
			//left = append(left, rangeList.Range[k:length]...)
			left = rangeList.Range[k:length]
			break
		}

		if compareResult == 0 {
			excludeRangeList := excludeRange(v, rangeElement)
			updateRangeList = append(updateRangeList, excludeRangeList...)
			continue
		}
	}

	updateRangeList = append(updateRangeList, left...)
	rangeList.Range = updateRangeList

	return nil
}

func (rangeList *RangeList) Print() string {
	var textRangeList []string

	for _, v := range rangeList.Range {
		textRange := fmt.Sprintf("[%d, %d)", v[0], v[1])
		textRangeList = append(textRangeList, textRange)
	}

	text := strings.Join(textRangeList, " ")
	fmt.Println(text)
	return text
}
