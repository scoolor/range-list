# RangeList

Implement a struct named 'RangeList'

A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4. A range list is an
aggregate of these ranges: [1, 5), [10, 11), [100, 201)
NOTE: Feel free to add any extra member variables/functions you like.

# Usage

## RangeList type

### func (*RangeList) Add

```
func (rangeList *RangeList) Add(rangeElement [2]int) error
```

### func (*RangeList) Remove

```
func (rangeList *RangeList) Remove(rangeElement [2]int) error
```

### func (*RangeList) Print

```
func (rangeList *RangeList) Print() string 
```

# Test

Run  `go test -v` to test

```
=== RUN   Test_Add1
[1, 5)
--- PASS: Test_Add1 (0.00s)
    rangelist_test.go:12: Pass
=== RUN   Test_Add2
[1, 5) [10, 20)
--- PASS: Test_Add2 (0.00s)
    rangelist_test.go:24: Pass
=== RUN   Test_Add3
[1, 5) [10, 20)
--- PASS: Test_Add3 (0.00s)
    rangelist_test.go:37: Pass
=== RUN   Test_Add4
[1, 5) [10, 21)
--- PASS: Test_Add4 (0.00s)
    rangelist_test.go:51: Pass
=== RUN   Test_Add5
[1, 5) [10, 21)
--- PASS: Test_Add5 (0.00s)
    rangelist_test.go:66: Pass
=== RUN   Test_Add6
[1, 8) [10, 21)
--- PASS: Test_Add6 (0.00s)
    rangelist_test.go:82: Pass
=== RUN   Test_Remove1
[1, 8) [10, 21)
--- PASS: Test_Remove1 (0.00s)
    rangelist_test.go:99: Pass
=== RUN   Test_Remove2
[1, 8) [11, 21)
--- PASS: Test_Remove2 (0.00s)
    rangelist_test.go:117: Pass
=== RUN   Test_Remove3
[1, 8) [11, 15) [17, 21)
--- PASS: Test_Remove3 (0.00s)
    rangelist_test.go:136: Pass
=== RUN   Test_Remove4
[1, 3) [19, 21)
--- PASS: Test_Remove4 (0.00s)
    rangelist_test.go:156: Pass
PASS
```



