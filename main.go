package main

import (
	rangeList "./rangelist"
	"fmt"
)

func main() {
	rl := rangeList.RangeList{}
	_ = rl.Add([2]int{1, 3})
	fmt.Println(rl.Print())

	_ = rl.Add([2]int{11, 13})
	fmt.Println(rl.Print())

	_ = rl.Add([2]int{8, 23})
	fmt.Println(rl.Print())

	_ = rl.Remove([2]int{18, 21})
	fmt.Println(rl.Print())

	_ = rl.Remove([2]int{1, 3})
	fmt.Println(rl.Print())
}
